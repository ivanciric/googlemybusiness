<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_SpecialHours extends Google_Collection
{
    protected $collection_key = 'specialHourPeriods';
    protected $internal_gapi_mappings = array(
    );
    protected $specialHourPeriodsType = 'Google_Service_MyBusiness_SpecialHourPeriod';
    protected $specialHourPeriodsDataType = 'array';


    public function setSpecialHourPeriods($specialHourPeriods)
    {
        $this->specialHourPeriods = $specialHourPeriods;
    }
    public function getSpecialHourPeriods()
    {
        return $this->specialHourPeriods;
    }
}
