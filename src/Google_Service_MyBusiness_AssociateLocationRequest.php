<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_AssociateLocationRequest extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $placeId;


    public function setPlaceId($placeId)
    {
        $this->placeId = $placeId;
    }
    public function getPlaceId()
    {
        return $this->placeId;
    }
}