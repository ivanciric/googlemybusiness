<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_PlaceInfo extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $name;
    public $placeId;


    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setPlaceId($placeId)
    {
        $this->placeId = $placeId;
    }
    public function getPlaceId()
    {
        return $this->placeId;
    }
}
