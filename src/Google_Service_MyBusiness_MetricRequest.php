<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_MetricRequest extends Google_Collection
{
    protected $collection_key = 'options';
    protected $internal_gapi_mappings = array(
    );
    public $metric;
    public $options;


    public function setMetric($metric)
    {
        $this->metric = $metric;
    }
    public function getMetric()
    {
        return $this->metric;
    }
    public function setOptions($options)
    {
        $this->options = $options;
    }
    public function getOptions()
    {
        return $this->options;
    }
}
