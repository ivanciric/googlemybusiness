<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_AccountState extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $status;


    public function setStatus($status)
    {
        $this->status = $status;
    }
    public function getStatus()
    {
        return $this->status;
    }
}