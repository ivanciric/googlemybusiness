<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_Reviewer extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $displayName;
    public $isAnonymous;


    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }
    public function getDisplayName()
    {
        return $this->displayName;
    }
    public function setIsAnonymous($isAnonymous)
    {
        $this->isAnonymous = $isAnonymous;
    }
    public function getIsAnonymous()
    {
        return $this->isAnonymous;
    }
}
