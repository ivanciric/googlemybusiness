<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_AttributeValueMetadata extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $displayName;
    public $value;


    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }
    public function getDisplayName()
    {
        return $this->displayName;
    }
    public function setValue($value)
    {
        $this->value = $value;
    }
    public function getValue()
    {
        return $this->value;
    }
}