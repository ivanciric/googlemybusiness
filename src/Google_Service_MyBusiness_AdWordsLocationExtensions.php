<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_AdWordsLocationExtensions extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $adPhone;


    public function setAdPhone($adPhone)
    {
        $this->adPhone = $adPhone;
    }
    public function getAdPhone()
    {
        return $this->adPhone;
    }
}