<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_UrlAttributeValue extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $url;


    public function setUrl($url)
    {
        $this->url = $url;
    }
    public function getUrl()
    {
        return $this->url;
    }
}
