<?php namespace ivanciric\Google;


class Google_Service_MyBusiness extends Google_Service
{


    public $accounts;
    public $accounts_admins;
    public $accounts_invitations;
    public $accounts_locations;
    public $accounts_locations_admins;
    public $accounts_locations_localPosts;
    public $accounts_locations_media;
    public $accounts_locations_media_customers;
    public $accounts_locations_reviews;
    public $attributes;
    public $categories;


    /**
     * Constructs the internal representation of the MyBusiness service.
     *
     * @param Google_Client $client
     */
    public function __construct(Google_Client $client)
    {
        parent::__construct($client);
        $this->rootUrl = 'https://mybusiness.googleapis.com/';
        $this->servicePath = '';
        $this->version = 'v4';
        $this->serviceName = 'mybusiness';

        $this->accounts = new Google_Service_MyBusiness_Accounts_Resource(
            $this,
            $this->serviceName,
            'accounts',
            array(
                'methods' => array(
                    'deleteNotifications' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'generateAccountNumber' => array(
                        'path' => 'v4/{+name}:generateAccountNumber',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'getNotifications' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/accounts',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'filter' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'name' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),'update' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PUT',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'validateOnly' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                        ),
                    ),'updateNotifications' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PUT',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_admins = new Google_Service_MyBusiness_AccountsAdmins_Resource(
            $this,
            $this->serviceName,
            'admins',
            array(
                'methods' => array(
                    'create' => array(
                        'path' => 'v4/{+parent}/admins',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'delete' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/admins',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'patch' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PATCH',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_invitations = new Google_Service_MyBusiness_AccountsInvitations_Resource(
            $this,
            $this->serviceName,
            'invitations',
            array(
                'methods' => array(
                    'accept' => array(
                        'path' => 'v4/{+name}:accept',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'decline' => array(
                        'path' => 'v4/{+name}:decline',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/invitations',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'targetType' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations = new Google_Service_MyBusiness_AccountsLocations_Resource(
            $this,
            $this->serviceName,
            'locations',
            array(
                'methods' => array(
                    'associate' => array(
                        'path' => 'v4/{+name}:associate',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'batchGet' => array(
                        'path' => 'v4/{+name}/locations:batchGet',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'clearAssociation' => array(
                        'path' => 'v4/{+name}:clearAssociation',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'create' => array(
                        'path' => 'v4/{+parent}/locations',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'validateOnly' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                            'requestId' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'delete' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'findMatches' => array(
                        'path' => 'v4/{+name}:findMatches',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'getGoogleUpdated' => array(
                        'path' => 'v4/{+name}:googleUpdated',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/locations',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'filter' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),'patch' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PATCH',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'validateOnly' => array(
                                'location' => 'query',
                                'type' => 'boolean',
                            ),
                            'attributeMask' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'updateMask' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'reportInsights' => array(
                        'path' => 'v4/{+name}/locations:reportInsights',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'transfer' => array(
                        'path' => 'v4/{+name}:transfer',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations_admins = new Google_Service_MyBusiness_AccountsLocationsAdmins_Resource(
            $this,
            $this->serviceName,
            'admins',
            array(
                'methods' => array(
                    'create' => array(
                        'path' => 'v4/{+parent}/admins',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'delete' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/admins',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'patch' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PATCH',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations_localPosts = new Google_Service_MyBusiness_AccountsLocationsLocalPosts_Resource(
            $this,
            $this->serviceName,
            'localPosts',
            array(
                'methods' => array(
                    'create' => array(
                        'path' => 'v4/{+parent}/localPosts',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'delete' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/localPosts',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),'patch' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PATCH',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'updateMask' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'reportInsights' => array(
                        'path' => 'v4/{+name}/localPosts:reportInsights',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations_media = new Google_Service_MyBusiness_AccountsLocationsMedia_Resource(
            $this,
            $this->serviceName,
            'media',
            array(
                'methods' => array(
                    'create' => array(
                        'path' => 'v4/{+parent}/media',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'delete' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/media',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),'patch' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'PATCH',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'updateMask' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),'startUpload' => array(
                        'path' => 'v4/{+parent}/media:startUpload',
                        'httpMethod' => 'POST',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations_media_customers = new Google_Service_MyBusiness_AccountsLocationsMediaCustomers_Resource(
            $this,
            $this->serviceName,
            'customers',
            array(
                'methods' => array(
                    'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/media/customers',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),
                )
            )
        );
        $this->accounts_locations_reviews = new Google_Service_MyBusiness_AccountsLocationsReviews_Resource(
            $this,
            $this->serviceName,
            'reviews',
            array(
                'methods' => array(
                    'deleteReply' => array(
                        'path' => 'v4/{+name}/reply',
                        'httpMethod' => 'DELETE',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'get' => array(
                        'path' => 'v4/{+name}',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),'list' => array(
                        'path' => 'v4/{+parent}/reviews',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'parent' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                            'orderBy' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                        ),
                    ),'updateReply' => array(
                        'path' => 'v4/{+name}/reply',
                        'httpMethod' => 'PUT',
                        'parameters' => array(
                            'name' => array(
                                'location' => 'path',
                                'type' => 'string',
                                'required' => true,
                            ),
                        ),
                    ),
                )
            )
        );
        $this->attributes = new Google_Service_MyBusiness_Attributes_Resource(
            $this,
            $this->serviceName,
            'attributes',
            array(
                'methods' => array(
                    'list' => array(
                        'path' => 'v4/attributes',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'languageCode' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'name' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                            'country' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'categoryId' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),
                )
            )
        );
        $this->categories = new Google_Service_MyBusiness_Categories_Resource(
            $this,
            $this->serviceName,
            'categories',
            array(
                'methods' => array(
                    'list' => array(
                        'path' => 'v4/categories',
                        'httpMethod' => 'GET',
                        'parameters' => array(
                            'searchTerm' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'regionCode' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageToken' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                            'pageSize' => array(
                                'location' => 'query',
                                'type' => 'integer',
                            ),
                            'languageCode' => array(
                                'location' => 'query',
                                'type' => 'string',
                            ),
                        ),
                    ),
                )
            )
        );
    }
}