<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_MediaItemDataRef extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $resourceName;


    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;
    }
    public function getResourceName()
    {
        return $this->resourceName;
    }
}
