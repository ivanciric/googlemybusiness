<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_ReviewReply extends Google_Model
{
    protected $internal_gapi_mappings = array(
    );
    public $comment;
    public $updateTime;


    public function setComment($comment)
    {
        $this->comment = $comment;
    }
    public function getComment()
    {
        return $this->comment;
    }
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    }
    public function getUpdateTime()
    {
        return $this->updateTime;
    }
}
