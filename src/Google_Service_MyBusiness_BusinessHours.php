<?php namespace ivanciric\Google;

class Google_Service_MyBusiness_BusinessHours extends Google_Collection
{
    protected $collection_key = 'periods';
    protected $internal_gapi_mappings = array(
    );
    protected $periodsType = 'Google_Service_MyBusiness_TimePeriod';
    protected $periodsDataType = 'array';


    public function setPeriods($periods)
    {
        $this->periods = $periods;
    }
    public function getPeriods()
    {
        return $this->periods;
    }
}